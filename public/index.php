<?php

require '../vendor/autoload.php';

require '../config/config.php';

use Illuminate\Database\Capsule\Manager as DB;

use \Firebase\JWT\JWT;

require '../src/loadModels.php';

date_default_timezone_set('Europe/Paris');

// CONFIG JWT
$app->jwtKey = "FDD7B6F4606C6C0B51159B9ED0143F92B40A15745BBD3A20CFF89509076AF2FB";

// Route de base juste pour afficher les infos du serveur php
$app->get('/', function ($request, $response) use ($app) {
  try {
      DB::connection()->getPdo();
      echo 'fzerze';
  } catch (\Exception $e) {
      echo $e;
  }
});

// Liste tous les utilisateurs
$app->get('/user', function ($request, $response) {

    $users = User::get();

    return $response->withJson($users);

});

// Créer un utilisateur
$app->post('/user', function ($request, $response) {

    $data = $request->getParam('user');

    try {
        $user = new \User();

        $user->nom = $data['nom'];
        $user->prenom = $data['prenom'];
        $user->email = $data['email'];
        $user->password = sha1($data['password']);

        $user->save();

        return $response->write($user->id);

    } catch (\Exception $e) {
        $newResponse = $response->withStatus(500);
        return $newResponse->write($e);
    }

});

// Login utilisateur
$app->post('user/connect', function ($request, $response) {

    $data = $request->getParam('user');



});

$app->run();