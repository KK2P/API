<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new Slim\Container($configuration);

$app = new \Slim\App($c);

// CORS ENABLE
    
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$container = $app->getContainer();

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => '54.37.148.100',
    'database' => 'nuitinfo',
    'username' => 'root',
    'password' => 'kk2p',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
    'options' =>
    []
]);

/*$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'mysql',
    'port' => '3306',
    'database' => 'test',
    'username' => 'root',
    'password' => 'R3!o@d3d',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);*/

$capsule->bootEloquent();
$capsule->setAsGlobal();